package com.pepper;

import java.util.Arrays;

public class SchemaJSONDTO {
	@Override
	public String toString() {
		return "SchemaJson [level=" + level + ", message=" + message + ", domain=" + domain + ", keyword=" + keyword
				+ ", found=" + found + ", expected=" + Arrays.toString(expected) + ", schema=" + schema + "]";
	}
	public String level;
	public String message;
	public String domain;
	public String keyword;
	public String found;
	public String[] expected;
	public Schema schema;
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getFound() {
		return found;
	}
	public void setFound(String found) {
		this.found = found;
	}
	public String[] getExpected() {
		return expected;
	}
	public void setExpected(String[] expected) {
		this.expected = expected;
	}
	public Schema getSchema() {
		return schema;
	}
	public void setSchema(Schema schema) {
		this.schema = schema;
	}
	
}
class Schema{
	public String loadingURI;
	public String pointer;
}
