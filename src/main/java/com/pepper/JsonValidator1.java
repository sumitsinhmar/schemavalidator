package com.pepper;

import org.mule.api.MessagingException;
import org.mule.api.MuleEvent;
import org.mule.exception.TemplateMessagingExceptionStrategy;
import org.mule.module.json.validation.JsonSchemaValidationException;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.core.exceptions.InvalidSchemaException;
import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.google.gson.Gson;

public class JsonValidator1 extends TemplateMessagingExceptionStrategy {
	public JsonValidator1() {
		setHandleException(true);
	}

	@Override
	protected void nullifyExceptionPayloadIfRequired(MuleEvent event) {
		event.getMessage().setExceptionPayload(null);
	}

	@Override
	protected MuleEvent afterRouting(Exception exception, MuleEvent event) {
		return event;
	}

	@Override
	protected MuleEvent beforeRouting(Exception exception, MuleEvent event) {
		MessagingException messagingException = (MessagingException) exception;
		JsonSchemaValidationException jsonSchemaValidationException = (JsonSchemaValidationException) messagingException.getCause();
		InvalidSchemaException invalidSchemaException = (InvalidSchemaException) jsonSchemaValidationException.getCause();
		ProcessingMessage processingMessage = invalidSchemaException.getProcessingMessage();
		JsonNode root = processingMessage.asJson();
		String[] errors = root.path("message").asText().split("Syntax errors:");
		SchemaJSONDTO[] schemaErrors = null;
		if(errors != null  && errors.length>0){
			schemaErrors = parseStringToJson(errors[1]);
		}
		
		return event;
	}

	private SchemaJSONDTO[] parseStringToJson(String jsonString) {
		Gson gson = new Gson();
		SchemaJSONDTO[] schemaErrors = gson.fromJson(jsonString, SchemaJSONDTO[].class);
		return schemaErrors;

	}

}